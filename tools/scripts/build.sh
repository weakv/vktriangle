#!/bin/bash
pushd $(dirname "$0")
mkdir ../../build && pushd ../../build
conan install ..
cmake ..
cmake --build .