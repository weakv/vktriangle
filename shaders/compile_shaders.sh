#!/bin/bash
DST_DIR="../tools/glsl"

if [ "$(ls -A $DST_DIR)" ]; then
    echo "-- [COMPILE_SHADERS.SH]: Tools are already installed..."
else
    echo "-- [COMPILE_SHADERS.SH]: Downloading tools..."
    mkdir -p $DST_DIR
    wget -q "https://storage.googleapis.com/shaderc/artifacts/prod/graphics_shader_compiler/shaderc/linux/continuous_clang_release/380/20220202-124409/install.tgz"
    echo "-- [COMPILE_SHADERS.SH]: Installing tools..."
    tar -xvf install.tgz install/bin/
    mv install/bin/* $DST_DIR/
    echo "-- [COMPILE_SHADERS.SH]: Cleaning up installation files..."
    rm -rf install*
fi

$DST_DIR/glslc shader.vert -o vert.spv
$DST_DIR/glslc shader.frag -o frag.spv