@echo off
IF EXIST "..\tools\glsl" (
    echo -- [COMPILE_SHADERS.BAT]: Tools are already installed...
) ELSE (
    md ..\tools\glsl
    echo -- [COMPILE_SHADERS.BAT]: Downloading tools...
    wget https://storage.googleapis.com/shaderc/artifacts/prod/graphics_shader_compiler/shaderc/windows/continuous_release_2017/384/20220202-124410/install.zip
    powershell -c Expand-Archive -Path install.zip -DestinationPath ../tools/glsl
    echo -- [COMPILE_SHADERS.BAT]: Cleaning up install files...
    del /s /q install.zip
)

echo -- [COMPILE_SHADERS.BAT]: Compiling shader code...
..\tools\glsl\install\bin\glslc.exe shader.vert -o vert.spv
..\tools\glsl\install\bin\glslc.exe shader.frag -o frag.spv