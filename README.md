# VkTriangle

This is a small C++ application that is used for the purpose of learning C++, CMake, Conan etc. The main idea is to use this codebase to create POC workflows for C++ development.

List of things that I'm planning to implement:
1) Unit tests for code (`catch2` branch), code coverage tools
2) Create Clang validator job
3) Change to Ninja as generator and profiling build process
4) Try to compile Windows binaries on Linux using Clang

## Windows:
For working with Windows, you need to make sure that you have following software:
* Visual studio
* CMake
* Python, Pip, Pipenv

In order to generate solution file, first you'll have to run `pipenv install` in order to get Conan package manager, after that create `build` directory with `md build` command. After creating build folder, go to that directory and run `pipenv run conan install ../` which will handle dependencies specified in `conanfile.txt`, followed up by `cmake ../` command to generate solution file together with download of tools that are necessary for work with this project, like GLSL shader compiler `glslc.exe`.
If you want to build solution from command line, you can always run `cmake --build ./ --config [Release|Debug]` from `build` directory.

## Linux:
For working with Linux (tested on Ubuntu 20.04) you need to install following software:
* CMake, make
* Python, Pip, Pipenv
* GCC

With the following libraries:

```libx11-dev libx11-xcb-dev libfontenc-dev libice-dev libsm-dev libxau-dev libxaw7-dev libxcomposite-dev libxcursor-dev libxdamage-dev libxdmcp-dev libxext-dev libxfixes-dev libxft-dev libxi-dev libxinerama-dev libxkbfile-dev libxmu-dev libxmuu-dev libxpm-dev libxrandr-dev libxrender-dev libxres-dev libxss-dev libxt-dev libxtst-dev libxv-dev libxvmc-dev libxxf86vm-dev xtrans-dev libxcb-render0-dev libxcb-render-util0-dev libxcb-xkb-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-keysyms1-dev libxcb-randr0-dev libxcb-shape0-dev libxcb-sync-dev libxcb-xfixes0-dev libxcb-xinerama0-dev xkb-data libxcb-dri3-dev uuid-dev libxcb-util-dev```

Procedure for generating makefiles is same as with the Windows procedure mentioned above.

**WARNING FOR PIPENV**

Pipenv is platform specific for conan, which results in failed pipenv install when swtiching from windows to linux or vice versa. In order to avoid that problem delete `Pipfile.lock` file and run `pipenv install` command again.

## Gitlab CI:
Since this is just a hobby project, I've created slim debian python image with dependencies that only builds linux executables. Dockerfile can be located under `tools/docker` directory.
